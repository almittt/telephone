<?php

namespace App\Services;

use App\Dto\CreatePeopleDto;
use App\Dto\CreatePhoneDto;
use App\Dto\UpdateDto;
use App\Repositories\PhonebookRepository;

class PhonebookService
{
    private $phonebookRepository;

    function __construct(PhonebookRepository $phonebookRepository) {
        $this->phonebookRepository = $phonebookRepository;
    }

    public function displayAll() {
        return $this->phonebookRepository->findAll();
    }

    public function displayUser(string $id) {
        return $this->phonebookRepository->findPeopleById($id);
    }


    public function createPeople(CreatePeopleDto $dto) {
        $this->phonebookRepository->addPeople($dto);
    }

    public function createPhone(CreatePhoneDto $dto) {
        $this->phonebookRepository->addPhone($dto);
    }

    public function editAll(UpdateDto $dto) {
        $this->phonebookRepository->updateAll($dto);
    }

    public function removeUser(string $id) {
        $this->phonebookRepository->deleteUser($id);
    }




}
