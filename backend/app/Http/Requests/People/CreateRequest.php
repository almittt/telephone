<?php

namespace App\Http\Requests\People;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required|string|max:255',
            'name' => 'required|string|max:255'
            //'phone' => 'required|string|phone:KZ'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'surname.required' => 'Введите фамилию!',
            'name.required'  => 'Введите имя!',
            'surname.string' => 'Фамилия должна быть строкой!',
            'name.string' => 'Имя должно быть строкой!',
            'surname.max' => 'Превышено количество допустимых символов!',
            'name.max' => 'Превышено количество допустимых символов!',

        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'surname' => 'фамилия',
            'name' => 'Имя',
        ];
    }
}
