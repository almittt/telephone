<?php

namespace App\Http\Requests\Main;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'surname' => 'required|string|max:255',
            'name' => 'required|string|max:255',
            "phone"    => "required|array",
            "phone.*"  => "required|string|phone:KZ",
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'surname.required' => 'Введите фамилию!',
            'name.required'  => 'Введите имя!',
            'phone.required'  => 'Введите телефон!',
            'surname.string' => 'Фамилия должна быть строкой!',
            'name.string' => 'Имя должно быть строкой!',
            'phone.string' => 'Телефон должен быть строкой!',
            'surname.max' => 'Превышено количество допустимых символов!',
            'name.max' => 'Превышено количество допустимых символов!',
            'phone.phone' => 'Невалидный номер телефона!',

        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'surname' => 'фамилия',
            'name' => 'Имя',
            'phone' => 'Телефон',
        ];
    }


}
