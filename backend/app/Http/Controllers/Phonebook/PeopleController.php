<?php

namespace App\Http\Controllers\Phonebook;

use App\Dto\CreatePeopleDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\People\CreateRequest;
use App\Services\PhonebookService;
use Illuminate\Http\Request;


class PeopleController extends Controller
{
    private $phonebookService;
    private $request;

    function __construct(PhonebookService $phonebookService, Request $request) {
        $this->phonebookService = $phonebookService;
        $this->request = $request;
    }

    public function create() {
        return view('create.people');
    }

    public function store(CreateRequest $request) {
        $dto = new CreatePeopleDto($request->surname, $request->name);
        $this->phonebookService->createPeople($dto);
        return redirect('/')->with('status', 'Пользователь добавлен!');
    }

    public function destroy() {
        $this->phonebookService->removeUser($this->request->id);
        return redirect('/')->with('status', 'Пользователь удален!');
    }


}
