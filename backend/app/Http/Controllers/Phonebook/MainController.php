<?php

namespace App\Http\Controllers\Phonebook;

use App\Dto\UpdateDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Main\UpdateRequest;
use App\Services\PhonebookService;
use Illuminate\Http\Request;

class MainController extends Controller
{
    private $phonebookService;
    private $request;

    function __construct(PhonebookService $phonebookService, Request $request) {
        $this->phonebookService = $phonebookService;
        $this->request = $request;
    }

    public function index() {
        $people = $this->phonebookService->displayAll();
        return view('index', [
            'people' => $people,
        ]);
    }

    public function edit() {
        $people = $this->phonebookService->displayUser($this->request->id);
        return view('edit.edit', [
            'id' => $this->request->id,
            'people' => $people,
        ]);
    }

    public function update(UpdateRequest $request) {
        $dto = new UpdateDto($request->all(), $this->request->id);
        $people = $this->phonebookService->editAll($dto);
        return redirect('/')->with('status', 'Справочник успешно обновлен!');
    }

}
