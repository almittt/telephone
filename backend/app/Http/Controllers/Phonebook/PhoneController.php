<?php

namespace App\Http\Controllers\Phonebook;

use App\Dto\CreatePhoneDto;
use App\Http\Controllers\Controller;
use App\Http\Requests\Phone\CreateRequest;
use App\Services\PhonebookService;
use Illuminate\Http\Request;


class PhoneController extends Controller
{
    private $phonebookService;
    private $request;

    function __construct(PhonebookService $phonebookService, Request $request) {
        $this->phonebookService = $phonebookService;
        $this->request = $request;
    }

    public function create() {
        $user = $this->phonebookService->displayUser($this->request->id);

        return view('create.phone', [
            'id' => $this->request->id,
            'user' => $user
        ]);
    }

    public function store(CreateRequest $request) {
        $dto = new CreatePhoneDto($request->phone, $this->request->id);
        $this->phonebookService->createPhone($dto);
        return redirect('/')->with('status', 'Телефон добавлен!');
    }

}
