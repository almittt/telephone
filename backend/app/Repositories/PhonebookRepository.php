<?php


namespace App\Repositories;

use App\Models\People;
use App\Models\Phone;
use App\Dto\CreatePeopleDto;
use App\Dto\CreatePhoneDto;
use App\Dto\UpdateDto;


class PhonebookRepository
{
    public function findAll() {
        return People::paginate(2);
    }

    public function findPeopleById(string $id) {
        return People::find($id);
    }


    public function addPeople(CreatePeopleDto $dto) {

        $people = new People;

        $people->surname = $dto->surname;
        $people->name = $dto->name;
        $people->save();
    }

    public function addPhone(CreatePhoneDto $dto) {

        $phone = new Phone;

        $phone->phone = $dto->phone;
        $phone->people_id = $dto->peopleId;
        $phone->save();
    }

    public function updateAll(UpdateDto $dto) {

        $people = People::find($dto->id);

        $people->surname = $dto->surname;
        $people->name = $dto->name;
        $people->save();

        foreach ($dto->phone as $phone => $val) {
            Phone::where('people_id', $dto->id)
                ->where('id', $phone)
                ->update(['phone' => $val]);
        }
    }

    public function deleteUser(string $id) {

        $people = People::destroy($id);
    }




}
