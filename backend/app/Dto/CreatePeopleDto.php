<?php


namespace App\Dto;


class CreatePeopleDto
{
    public $surname;
    public $name;
    //public $phone;

    function __construct(string $surname, string $name  /*string $phone*/)
    {
        $this->surname = $surname;
        $this->name = $name;
        //$this->phone = $phone;
    }
}
