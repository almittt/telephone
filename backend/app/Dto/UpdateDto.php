<?php


namespace App\Dto;


class UpdateDto
{
    public $id;
    public $surname;
    public $name;
    public $phone;

    function __construct(array $request, string $id)
    {
        $this->id = $id;
        $this->surname = $request['surname'];
        $this->name = $request['name'];
        $this->phone = $request['phone'];
    }
}
