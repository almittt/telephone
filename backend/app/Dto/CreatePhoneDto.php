<?php
namespace App\Dto;


class CreatePhoneDto
{
    public $peopleId;
    public $phone;

    function __construct(string $phone, string $peopleId)
    {
        $this->phone = $phone;
        $this->peopleId = $peopleId;
    }
}
