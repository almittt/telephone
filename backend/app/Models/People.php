<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'people';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';


    /**
     * Get the phone record associated with the user.
     */
    public function phones()
    {
        return $this->hasMany('App\Models\Phone', 'people_id', 'id');
    }

}
