@extends('adminlte::page')

@section('title', 'Телефонный справочник')

@section('content_header')
    <h1>Телефонный справочник</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Редактировать</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="{{route('update', ['id' => $id])}}" role="form">
            @csrf
            @method('put')
            <div class="card-body">
                <div class="form-group">
                    <label for="surname">Фамилия</label>
                    <input type="text" class="form-control" id="surname" name="surname" placeholder="Фамилия" value="{{$people->surname}}">
                    @if($errors->has('surname'))
                        <div class="text-danger">{{ $errors->first('surname') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Имя" value="{{$people->name}}">
                    @if($errors->has('name'))
                        <div class="text-danger">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <p><b>Телефоны</b></p>
                    @foreach($people->phones as $phone)
                        <input type="text" class="form-control"  name="phone[{{$phone->id}}]" placeholder="Телефон" value="{{ $phone->phone}}"><br>
                        @if($errors->has('phone{{$i}}'))
                            <div class="text-danger">{{ $errors->first('phone') }}</div>
                        @endif
                    @endforeach
                </div>
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </form>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

