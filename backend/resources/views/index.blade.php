@extends('adminlte::page')

@section('title', 'Телефонный справочник')

@section('content_header')
    <h1>Телефонный справочник</h1>
@stop

@section('content')
    @if (session('status'))
        <div class="alert alert-success">
            {{ session('status') }}
        </div>
    @endif
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Список номеров</h3>
        </div>
        <div class="card-body">
            <table class="table table-bordered">
                <tbody><tr>
                    <th style="width: 10px">#</th>
                    <th>Фамилия</th>
                    <th>Имя</th>
                    <th>Телефоны</th>
                    <th style="width: 40px">Действие</th>
                </tr>
                @if($people)
                    @php($i = 1)
                    @foreach($people as $man)
                    <tr>
                        <td>{{$i}}</td>
                        <td>{{$man->surname}}</td>
                        <td>{{$man->name}}</td>
                        <td>
                            @foreach($man->phones as $phone)
                                {{$phone->phone}}<br>
                            @endforeach
                        </td>
                        <td>
                            <a href="{{route('phone.create', ['id' => $man->id])}}" class="btn btn-primary"><i class="far fa-fw fa-file"></i></a>
                            <a href="{{route('edit', ['id' => $man->id])}}" class="btn btn-success"><i class="far fa-fw fa-edit"></i></a>
                            <form method="post" action="{{route('people.remove', ['id' => $man->id])}}">
                                @csrf
                                @method('delete')
                                <button type="submit" class="btn btn-danger"><i class="fas fa-fw fa-ban"></i></button>
                            </form>
                        </td>
                    </tr>
                    @php($i = $i+1)
                    @endforeach
                 @endif
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix">
            <ul class="pagination pagination-sm m-0 float-right">
                {{ $people->links() }}
            </ul>
        </div>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

