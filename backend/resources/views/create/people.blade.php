@extends('adminlte::page')

@section('title', 'Телефонный справочник')

@section('content_header')
    <h1>Телефонный справочник</h1>
@stop

@section('content')
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Добавить пользователя</h3>
        </div>
        <!-- /.card-header -->
        <!-- form start -->
        <form method="post" action="{{route('people.store')}}" role="form">
            @csrf
            @method('post')
            <div class="card-body">
                <div class="form-group">
                    <label for="surname">Фамилия</label>
                    <input type="text" class="form-control" id="surname" name="surname" placeholder="Фамилия" value="{{ old('surname')}}">
                    @if($errors->has('surname'))
                        <div class="text-danger">{{ $errors->first('surname') }}</div>
                    @endif
                </div>
                <div class="form-group">
                    <label for="name">Имя</label>
                    <input type="text" class="form-control" id="name" name="name" placeholder="Имя" value="{{ old('name')}}">
                    @if($errors->has('name'))
                        <div class="text-danger">{{ $errors->first('name') }}</div>
                    @endif
                </div>
{{--                <div class="form-group">--}}
{{--                    <label for="phone">Телефон</label>--}}
{{--                    <input type="phone" class="form-control" id="phone" name="phone" placeholder="Телефон" value="{{ old('phone')}}">--}}
{{--                    @if($errors->has('phone'))--}}
{{--                        <div class="text-danger">{{ $errors->first('phone') }}</div>--}}
{{--                    @endif--}}
{{--                </div>--}}
            </div>
            <!-- /.card-body -->

            <div class="card-footer">
                <button type="submit" class="btn btn-primary">Добавить</button>
            </div>
        </form>
    </div>
@stop

@section('css')
    <link rel="stylesheet" href="/css/admin_custom.css">
@stop

