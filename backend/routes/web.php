<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Phonebook\MainController@index')->name('main');
Route::get('edit/{id}', 'Phonebook\MainController@edit')->name('edit');
Route::get('people/add', 'Phonebook\PeopleController@create')->name('people.create');
Route::get('phone/add', 'Phonebook\PhoneController@create')->name('phone.create');
Route::post('people/store', 'Phonebook\PeopleController@store')->name('people.store');
Route::post('people/store/{id}', 'Phonebook\PhoneController@store')->name('phone.store');
Route::delete('people/remove', 'Phonebook\PeopleController@destroy')->name('people.remove');
Route::put('update/{id}', 'Phonebook\MainController@update')->name('update');


